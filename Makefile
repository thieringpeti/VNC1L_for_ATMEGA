MCU=atmega2561
 
CC=avr-gcc
OBJCOPY=avr-objcopy
# optimize for size:
CFLAGS=-g -mmcu=$(MCU) -Wall -Wstrict-prototypes -Os -mcall-prologues
PORT=/dev/ttyUSB0
PART=atmega2561
UISP = uisp -dprog=stk500 -dserial=/dev/ttyUSB0 -dpart=$(PART)

VERSION = 0.1
DISTDIR = avr-midi.${VERSION}

#-------------------
current: main.hex
#-------------------
F_CPU = 16000000


mainSRC = example.c serial.c



mainOBJ = ${mainSRC:.c=.o}
ECHOOBJ = ${ECHOSRC:.c=.o}

main.bin : main.out
	$(OBJCOPY) -R .eeprom -O binary main.out main.bin
	
main.hex : main.out 
	$(OBJCOPY) -R .eeprom -O ihex main.out main.hex 

main.out : $(mainOBJ)
	$(CC) $(CFLAGS) -o main.out -Wl,-Map,main.map $(mainOBJ)
	avr-size -C --mcu=$(PART) main.out  


echo.bin : echo.out
	$(OBJCOPY) -R .eeprom -O binary echo.out echo.bin 

echo.hex : echo.out 
	$(OBJCOPY) -R .eeprom -O ihex echo.out echo.hex 

echo.out : $(ECHOOBJ)
	$(CC) $(CFLAGS) -o echo.out -Wl,-Map,echo.map $(ECHOOBJ)


dist: clean
	mkdir -p ${DISTDIR}
	cp -R COPYING Makefile *.c *.h README ${DISTDIR}
	tar -czf ${DISTDIR}.tar.gz --exclude=".svn" ${DISTDIR}
	rm -rf ${DISTDIR}

post: dist
	scp ${DISTDIR}.tar.gz x37v.info:x37v.info/projects/microcontroller/avr-midi/files/
	scp main.c x37v.info:x37v.info/projects/microcontroller/avr-midi/

.c.o:
	@echo CC $<
	@$(CC) -c $(CFLAGS) -Os -o $*.o $<

# load (program) the software
load_main: main.hex
	$(UISP) --erase
	$(UISP) --upload --verify if=main.hex -v=3 --hash=32

load_echo: echo.hex
	$(UISP) --erase
	$(UISP) --upload --verify if=echo.hex -v=3 --hash=32

fuse_mega16:
	$(UISP) --wr_fuse_l=0x9f --wr_fuse_h=0xd0

check_fuse:
	$(UISP) --rd_fuses

#-------------------
clean:
	rm -f *.o *.map *.out *.hex *.tar.gz
#-------------------
