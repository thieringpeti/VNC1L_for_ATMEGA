This project is a fixed version of an old VNC1L driver for ATMEGA microcontrollers via SPI interface.

It contains a demo, which writes some messages to the USB thumb drive. 

You can read more about VNC1L - AVR interfacing here:

https://rtime.felk.cvut.cz/hw/index.php/USB_VNC1L_module

Original (adopted, modified and fixed) source codes from a university design project there:

https://people.ece.cornell.edu/land/courses/eceprojectsland/STUDENTPROJ/2012to2013/yk579/

Have fun with it.