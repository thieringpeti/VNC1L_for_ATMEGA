#define LED_PIN 4 
#define SCLK_PIN 1 
#define SDI_PIN 2 
#define SDO_PIN 3 
#define CS_PIN 0 
#define RE_PIN 7 
 
#define LED_PORT PORTD 
#define SPI_PORT PORTB // PORTD 
 
#define LED_DDR DDRD 
#define SPI_DDR DDRB // DDRD 
#define SPI_PIN PINB // PIND 

 
//************************************************************************ 
//************************************************************************ 
#define LED_D1_ON LED_PORT &= ~(1<<LED_PIN) /* LED */ 
#define LED_D1_OFF LED_PORT |= (1<<LED_PIN) 
 
#define SCLK_L SPI_PORT &= ~(1<<SCLK_PIN) /* SCLK VNC1L */ 
#define SCLK_H SPI_PORT |= (1<<SCLK_PIN) 
 
#define SDI_L SPI_PORT &= ~(1<<SDI_PIN) /* SDI VNC1L */ 
#define SDI_H SPI_PORT |= (1<<SDI_PIN) 
 
#define SDO_VNC (SPI_PIN & (1<<SDO_PIN)) /* SDO VNC1L */ 
 
#define CS_VNC_L SPI_PORT &= ~(1<<CS_PIN) /* CS VNC1L */ 
#define CS_VNC_H SPI_PORT |= (1<<CS_PIN) 
 
#define RE_VNC_L SPI_PORT &= ~(1<<RE_PIN) /* RS VNC1L */ 
#define RE_VNC_H SPI_PORT |= (1<<RE_PIN) 
 
#define PROMPT_CHECK 2 
#define PROMPT_AND_ERROR_CHECK 1 
#define PROMPT_OK 1 
#define FILE_NEW 0 
#define FILE_APPEND 1 
#define TIMEOUT_SPI 50 /* Number of SPI read/write tests */ 
#define TIMEOUT_READ_WRITE 500 /* Number of read/write tests if 
buffer is full */ 
#define TIMEOUT_COMMUNICATION 5 /* Timeout if no communication has 
happened */ 
 
#define FILE_BUFFER_SIZE 100 /* Buffer size is at least 100 */ 

