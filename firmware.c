
//****************** firmware.c ******************* 

#include "firmware.h" // defines macros 
#include "serial.h"
#include <avr/io.h> 
#define VNC_BUFFER_SIZE 60 /* Transmit/receive buffer size is at least 60 */ 
 
struct vnc_struc 
{ 
 char receive_buffer[VNC_BUFFER_SIZE]; // Receive buffer
 char send_buffer[VNC_BUFFER_SIZE]; // Send buffer
 unsigned char status; // Status 
}; 

struct vnc_struc vnc; // instantiate vnc_struc 
 
 
// *********************************************************************** 
// VNC1L - Initialize SPI-Interface and VNC1L 
// SPI Mode 
// *********************************************************************** 

void vnc_init(void) 
{ 
 CS_VNC_L; // SPI-Interface inactive 
 SCLK_L; // idle status of SPI lines 
 SDI_H; 
 RE_VNC_L; // Hardware-reset VNC1L 
 _delay_ms(100); // Pause 
 RE_VNC_H; // start VNC1L 
 _delay_ms(500); // Pause 
} 
 
// *********************************************************************** 
// VNC1L - 13-Bit-SPI Sending a character 
// SPI Mode 
// Parameter: wr_byte : character to be output 
// Return: status : 0 = Accepted (OK), 1 = Byte is not accepted by VNC1L 
// *********************************************************************** 

unsigned char vnc_wr_spi(unsigned char wr_byte) 
{ 
 unsigned char bit_pos, status, count; 
 
 count = TIMEOUT_SPI; // timeout after unsuccessful number of attempts to write 
 
 do { 
 CS_VNC_H; // SPI-Interface active 
 SDI_H; // Start bit (always 1) 
 SCLK_H; SCLK_L; // falling/rising edge 
 SDI_L; // Read/Write bit (0 = Write) 
 SCLK_H; SCLK_L; 
 SDI_L; // ADDR (0 = Data) 
 SCLK_H; SCLK_L; 
 
 // send 8 data bits (MSB first) 
 for ( bit_pos = 0x80; bit_pos; bit_pos >>= 1 ) 
 { 
 if (wr_byte & bit_pos) SDI_H; else SDI_L;  
 
 SCLK_H; SCLK_L; 
 } 
 // read status 
 status = SDO_VNC; // Status bit (0 = Accepted, 1 = Rejected) 
 SCLK_H; SCLK_L; 
 // End sequence 
 CS_VNC_L; // SPI-Interface inactive 
 SCLK_H; SCLK_L; 
 
 count--; 
 
 if (status) _delay_us(150); // Pause if a byte transfer has not be confirmed 
 } while (status && count); 
 
/* 
#ifdef USART_MONITORING 
 fprintf(stdout, "%c", wr_byte); 
#endif 
#ifdef LCD_MONITORING 
 sprintf(lcd_buffer, "%s", wr_byte); 
 LCDGotoXY(0, 0); 
 LCDstring(lcd_buffer, strlen(lcd_buffer)); 
#endif 
*/ 

return status; // 0: no error 
} 
 
// *********************************************************************** 
// VNC1L - 13-Bit-SPI receiving a character 
// SPI Mode 
// Parameter: rd_byte : type of read byte (0 = Data byte, 1 = Status byte) 
// Store the data byte in the vnc.receive_buffer of vnc struct variable 
// Return: status : 0 = new byte in the buffer (OK), 1 = a new byte is not available from VNC1L 
// *********************************************************************** 
unsigned char vnc_rd_spi(unsigned char rd_byte) 
{ 
 unsigned char bit_pos, data, status; 
 unsigned int count; 
 
 count = TIMEOUT_SPI; // timeout after unsuccessful number of attempts to write 
 
 do { 
 data = 0; 
 CS_VNC_H; // SPI-Interface active 
 
 SDI_H; // Start bit (always 1) 
 SCLK_H; SCLK_L; // falling/rising edge 
 SDI_H; // Read/Write bit (1 = Read) 
 SCLK_H; SCLK_L; 
 
 if (rd_byte != 0) SDI_H; else SDI_L; // ADDR (0 = Data, 1 = SPI Status information) 
 SCLK_H; SCLK_L; 
 
 // receive 8 data bits (MSB first) 
 for ( bit_pos = 0x80; bit_pos; bit_pos >>= 1 ) 
 { 
 if (SDO_VNC != 0) data |= bit_pos; // store data bits 
 SCLK_H; SCLK_L; 
 } 
 
 // read status 
 status = SDO_VNC; // Status bit (0 = New Data, 1 = Old Data) 
 SCLK_H; SCLK_L; 
 // End sequence 
 CS_VNC_L; // SPI-Interface inactive 
 SCLK_H; SCLK_L; 

 
 if (status == 0) { 
	 
 // received character is a new character 
 // make room in the vnc.receive_buffer for new data received 
 for ( bit_pos = VNC_BUFFER_SIZE - 1; bit_pos; bit_pos-- ) 
 vnc.receive_buffer[bit_pos] = vnc.receive_buffer[bit_pos - 1]; 
 vnc.receive_buffer[0] = data;
 // new charater is store in the first spot of the buffer (backwards) 
 
/* 
#ifdef USART_MONITORING 
 if (!rd_byte) { 
 fprintf(stdout, "%c", vnc.receive_byte[0]); 
 } 
#endif 
#ifdef LCD_MONITORING 
 if (!rd_byte) { 
 sprintf(lcd_buffer, "%s", vnc.receive_byte[0]); 
 LCDGotoXY(0, 0); 
 LCDstring(lcd_buffer, strlen(lcd_buffer)); 
 } 
#endif 
*/ 
 
 } 
 count--; 
 if (status) _delay_us(150); // pause if there is not a new byte 
 } while (status && count); 
 
 return status; 
} 
 
// *********************************************************************** 
// VNC1L - Sending a character 
// Parameter: wr_byte : character to be output 
// Return: status : 0 = Accepted (OK), 1 = Not accepted by VNC1L 
// *********************************************************************** 
unsigned char vnc_wr_byte(unsigned char wr_byte) 
{ 
 unsigned char status; 
 unsigned int count; 
 
 count = TIMEOUT_READ_WRITE; // number of attempts to send before timeout 
 
 do { 
 // read status byte from VNC1L 
 if (vnc_rd_spi(1)) return 1; // error, byte is not accepted 
 // Status byte is now in the buffer 
 status = vnc.receive_buffer[0] & 0x01; // test for VNC1L RECEIVE BUFFER full (RXF# Bit0=1) 
 count--; 
 if (status) _delay_us(150); // Pause if RECEIVE BUFFER full (RXF#) 
 } while (status && count); 
 if (status || (!count)) return 1; // error, byte is not accepted 
 // VNC1L is now ready to receive 
 if (vnc_wr_spi(wr_byte)) return 1; // error, byte is not accepted 
 
 return 0; // byte has been sent 
} 
 
// *********************************************************************** 
// VNC1L - Receiving a character 
// Return: status : 0 = New byte in the buffer (OK), 1 = a new byte is not available from VNC1L 
// *********************************************************************** 
unsigned char vnc_rd_byte(void) 
{ 
 unsigned char status;
 
 unsigned int count; 
 
 count = TIMEOUT_READ_WRITE; // number of attempts to receive before timeout 
 
 do { 
 // read status byte from VNC1L 
 status = vnc_rd_spi(0); // If status = 1, there is not a new byte 
 // Testing for TXE # in the status byte has no advantage 
 count--; 
 if (status) _delay_us(150); // Pause if there is no new byte 
 } while (status && count); 
 if (status || (!count)) return 1; // error, byte is not received 
 
 return 0; // byte is received 
} 
 
// *********************************************************************** 
// VNC1L - Sending a command 
// Parameter: wr_cmd : command (address) 
// Return: status : 0 = OK, 1 = command not accepted by VNC1L 
// *********************************************************************** 
unsigned char vnc_wr_cmd(char *wr_cmd) 
{ 
 unsigned char offset; // vnc_offset, length of command 
 
 for(offset = strlen(wr_cmd); offset; offset--) 
 { 
 if (vnc_wr_byte(*wr_cmd)) return 1; // error (overflow), command was not accepted 
 wr_cmd++; 
 } 
 // send carrage return (a command to monitor ends with 0x0D) 
 if (vnc_wr_byte(0x0D)) return 1; // error (overflow), command was not accepted 
 
 return 0; 
} 
 
// *********************************************************************** 
// VNC1L - Receive a string 
// Wait until the monitor returns info from firmware (ex. Firmware Version, Device Detected) 
// Last 12 digits are significant 
// Parameter: wr_string : String (address) 
// Return: status : 0 = OK, 1 = string was not received from VNC1L 
// *********************************************************************** 
unsigned char vnc_wait_for(char *wr_string) 
 
{ 
 unsigned char length; // String length / Offset 
 unsigned char match_index; // number of matches 
 unsigned char buff_index; // Buffer index 
 unsigned char i; 
 unsigned char status; 
 unsigned int rd_answer_count; 
 
 rd_answer_count = TIMEOUT_COMMUNICATION; // TIMEOUT error / abort if necessary 
 status = 0; 
 match_index = 0; 
 buff_index = 0; 
 length = strlen(wr_string); 
 
 if (length > 12) length = 12; // length limit 
 
 while(rd_answer_count && (length != match_index)) { 
 status = vnc_rd_byte(); 
 if (!status) { 
 // A new character is now available 
 buff_index = 0; 
 match_index = 0; 
 
 for(i = 0; i < length; i++) 
 { 
 if (vnc.receive_buffer[length - i] == *(wr_string + i)) 
 match_index++; // match 
 buff_index++; 
 } 
 
 rd_answer_count = TIMEOUT_COMMUNICATION; // reset timeout count 
 } else rd_answer_count--; 
 } 
 
 if (status || (match_index != length)) return 1; // error, string is not received 
 
 return 0; // String was received OK 
} 
 
// *********************************************************************** 
// VNC1L - Wait for response string and parse it 
// The last 4 digits of the answer are significant. (possibly expand it) 
// Parameter: buffer_check 
// 1 = Testing for prompt and error messages 51 
 
// 2 = Testing only for prompt 
// Return: reply 
// 1 = Prompt received 
// 2 = Command Failed received 
// 3 = Bad Command 
// 4 = Disk Full 
// 5 = Filename Invalid 
// 6 = Read Only 
// 7 = File Open 
// 8 = Directory Not Empty 
// 9 = No Disk 
// 99 = Timeout 
// *********************************************************************** 
unsigned char vnc_rd_answer(unsigned char buffer_check) 
{ 
 unsigned char reply; 
 unsigned char status; 
 unsigned int rd_answer_count; 
 
 rd_answer_count = TIMEOUT_COMMUNICATION; // TIMEOUT error / abort if necessary 
 reply = 0; 
 
 for (status = VNC_BUFFER_SIZE - 1; status; status-- ) 
 vnc.receive_buffer[status] = 0; // clear buffer (using status variable) 
 
 while(rd_answer_count) // not reached timeout limit 
 { 
 status = vnc_rd_byte(); 
 
 if (!status) 
 { 
 // new character is now available 
 switch (buffer_check) 
 { 
 case 1: // Command Failed 
 if ((vnc.receive_buffer[3] == 'i') && (vnc.receive_buffer[2] == 'l') && 
(vnc.receive_buffer[1] == 'e') && (vnc.receive_buffer[0] == 'd')) reply = 2; 
 // Bad Command 
 if ((vnc.receive_buffer[3] == 'm') && (vnc.receive_buffer[2] == 'a') && 
(vnc.receive_buffer[1] == 'n') && (vnc.receive_buffer[0] == 'd')) reply = 3; 
 // Disk Full 
 if ((vnc.receive_buffer[3] == 'F') && (vnc.receive_buffer[2] == 'u') && 
(vnc.receive_buffer[1] == 'l') && (vnc.receive_buffer[0] == 'l')) reply = 4; 
 // Invalid Filename 
 
 if ((vnc.receive_buffer[3] == 'a') && (vnc.receive_buffer[2] == 'l') && 
(vnc.receive_buffer[1] == 'i') && (vnc.receive_buffer[0] == 'd')) reply = 5; 
 // Read Only 
 if ((vnc.receive_buffer[3] == 'O') && (vnc.receive_buffer[2] == 'n') && 
(vnc.receive_buffer[1] == 'l') && (vnc.receive_buffer[0] == 'y')) reply = 6; 
 // File Open 
 if ((vnc.receive_buffer[3] == 'O') && (vnc.receive_buffer[2] == 'p') && 
(vnc.receive_buffer[1] == 'e') && (vnc.receive_buffer[0] == 'n')) reply = 7; 
 // Directory Not Empty 
 if ((vnc.receive_buffer[3] == 'm') && (vnc.receive_buffer[2] == 'p') && 
(vnc.receive_buffer[1] == 't') && (vnc.receive_buffer[0] == 'y')) reply = 8; 
 // No Disk 
 if ((vnc.receive_buffer[3] == 'D') && (vnc.receive_buffer[2] == 'i') && 
(vnc.receive_buffer[1] == 's') && (vnc.receive_buffer[0] == 'k')) reply = 9; 
 // Here you can check for other error messages ... 
 // No 'break;' here!! After error check, prompt check should be done as well 
 
 case 2:// Only Prompt Check 
 if ((vnc.receive_buffer[2] == 0x5C) && (vnc.receive_buffer[1] == '>') && 
(vnc.receive_buffer[0] == 0x0D)) 
 { 
 // Test for \>, Carriage Return is received 
 reply = 1; 
 // vnc.receive_buffer[0] = 0; // Buffer discard (delete the last character from the last string!) 
 } 
 } 
 
 if (reply >= 2) 
 { 
 if(vnc_wr_byte(0x0D)) return 0; // send Carriage Return to confirm error, then comes a prompt, exit on error 
 } 
 
 if (reply) break; 
 
 rd_answer_count = TIMEOUT_COMMUNICATION; // Timeout reset 
 } else rd_answer_count--; 
 } 
 
 if (!reply) reply = 99; // Timeout 
 
 return reply;  
 
} 
 
// *********************************************************************** 
// VNC1L - Waiting for PROMPT 
// If there is an error, return the error number 
// Return: 0 = Prompt received 
// xx = error number from vnc_rd_answer function 
// *********************************************************************** 
unsigned char vnc_prompt_check(void) 
{ 
 unsigned char status_1, status_2; 
 
 status_1 = vnc_rd_answer(PROMPT_AND_ERROR_CHECK); 
 
 if (status_1 == 1) return 0; // Prompt was received, no previous error, command was executed 
 
 // Error occurred, now just waiting for prompt, but confirming error 
 status_2 = vnc_rd_answer(PROMPT_CHECK); 
 
 if (status_2 == 1) return status_1; 
 
 return status_2; // no Prompt, Timeout 
} 
 
// *********************************************************************** 
// VNC1L - Creating a text file on a USB drive 
// Maximum file length: 64 kByte 
// Parameters: file_name : address of file_name 
// file_content : address of text block to be written 
// option : File 0 = new or 1 = append content 
// Return: status : error number (0 = OK) 
// *********************************************************************** 
unsigned char vnc_wr_txtfile(char *file_name, char *file_content, char option) 
{ 
 unsigned int vnc_i = 0; 
 unsigned int file_length = 0; 
 unsigned int file_CR = 0; // carriage return 
 char *file_ptr; 
 unsigned char wr_steps = 0; 
 unsigned char status = 0; 
 
 // calculate number of bytes 
 file_ptr = file_content;  
 
 file_length = 0; 
 file_CR = 0; 
 
 while (*file_ptr != '\a') // '\a' is an identifier for the end of text 
 { 
 if (*file_ptr) file_length++; else file_CR++; 
 file_ptr++; 
 } 
 
 do { 
 if (wr_steps == 4) return 0; // Done, OK (last prompt character exists) 
 
 switch (wr_steps) // write process 
 { 
 case 0: // Open or create file 
 sprintf (&vnc.send_buffer[0], "OPW %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 break; 
 
 case 1: // Seek offset byte position of 0 in the open file 
 if (!option) // new file 
 { 
 if (vnc_wr_cmd("SEK 0")) return 1; // Error, command was not accepted 
 break; 
 } 
 else wr_steps++; // skip to case 2 (append at the end of existing file) 
 
 case 2: // Write the number of bytes to an open file 
 sprintf (&vnc.send_buffer[0], "WRF %u", file_length); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 file_ptr = file_content; 
 
 for(vnc_i = 0; vnc_i < (file_length + file_CR); vnc_i++) 
 { 
 if (*file_ptr) 
 { 
 // hide NULL termination 
 if (vnc_wr_byte(*file_ptr)) return 1; // Error, command was not accepted 
 } 
 file_ptr++; 
 } 
 break; 
  
 
 case 3: // Close file 
 sprintf (&vnc.send_buffer[0], "CLF %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 break; 
 } // Switch 
 
 wr_steps++; // next step 
 status = vnc_rd_answer(PROMPT_AND_ERROR_CHECK); 
 } while (status == PROMPT_OK); // Testing for next prompt character > 
 
 return status; // error number (while 0 = OK) 
} 
 
// *********************************************************************** 
// VNC1L - Reading a file of a specific length from an USB drive 
// Length of the file should be known, so determine it beforehand with vnc_rd_dir! 
// Parameters: file_name : address of file_name 
// file_content : address of destination buffer where the read data will be stored 
// file_length : number of bytes 
// Return: status : error number, 0 = OK 
// *********************************************************************** 
unsigned char vnc_rd_file(char *file_name, char *file_content, unsigned int file_length) 
{ 
 unsigned char rd_steps = 0; 
 unsigned char status = 0; 
 
 if (file_length >= (FILE_BUFFER_SIZE - 1)) 
 { 
 return 50; // error, buffer is too small 
 } 
 
 do { 
 if (rd_steps == 4) return 0; // Done, OK (last prompt character exists) 
 
 switch (rd_steps) // read process 
 { 
 case 0: // Open file for reading 
 sprintf (&vnc.send_buffer[0], "OPR %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 break; 
 
 case 1: // Seek offset byte position of 0 in the open file (you can also choose other offset) 
 if (vnc_wr_cmd("SEK 0")) return 1; // Error, command was not accepted 
 break;  
 
 
 case 2: // Read the number of bytes from an open file 
 sprintf (&vnc.send_buffer[0], "RDF %d", file_length); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 
 while (file_length) 
 { 
 if (vnc_rd_byte()) return 1; // error, byte is not received 
 // new character exists 
 *file_content = vnc.receive_buffer[0]; 
 file_content++; 
 file_length--; 
 } 
 // The characters read are now in the buffer and can be evaluated. 
 break; 
 
 case 3: // close file 
 sprintf (&vnc.send_buffer[0], "CLF %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 break; 
 } // Switch 
 rd_steps++; // next step 
 status = vnc_rd_answer(PROMPT_AND_ERROR_CHECK); 
 } while (status == PROMPT_OK); // Testing for next prompt character > 
 
 return status; // error number (while 0 = OK) 
} 
 
// *********************************************************************** 
// VNC1L - Convert hex string to a number 
// Example: "$11 $22 $33 $44 D:\>" --> 0x44332211 
// Maximum file length: 64 kByte (easily expandable to 32-bit) 
// Parameter: value : Address of hex string 
// Return: file_length (16 bit), 0 = error 
// *********************************************************************** 
unsigned int str_to_int(char *value) 
{ 
 unsigned char temp[4]; 
 unsigned char position = 0; 
 unsigned char nibble = 0; 
 unsigned char error = 0; 
 unsigned char mid_byte = 0; 
 
 for (position = 4; position; position--)
 
 { 
 if ( (*value == '0') && ( *(value + 1) == '$') ) value += 2; // skip $00 
 if (*value == '$') value += 1; // skip $ 
 mid_byte = 4; 
 temp[position - 1] = 0; 
 
 for (nibble = 2; nibble; nibble--) 
 { 
 if ((*value >= 'a') && (*value <= 'f')) *value = *value - 0x20; // Convert to uppercase 
 if ((*value >= 'A') && (*value <= 'F')) *value = *value - 0x07; // normalize (hex based) 
 if ((*value < '0') || (*value > '?')) error = 1; // error 
 temp[position - 1] |= (*value - 0x30) << mid_byte; // string to a number (subtract 0x30 from a character 0-9) 
 mid_byte -= 4; // next lower 4 bits out of a byte 
 value++; 
 } 
 value++; 
 } 
 
 if ((temp[1] != 0) || (temp[0] != 0 )) error = 1; // error > 16-bit 
 if (error) 
 { 
 // *(value - 16) = 0x00; // No response necessary via buffer 
 temp[2] = 0; 
 temp[3] = 0; 
 } 
 
 return temp[2] * 256 + temp[3]; 
} 
 
// *********************************************************************** 
// VNC1L - DIR determines file length 
// Maximum 64 kByte!!! 
// Parameters: file_name : Address of file_name 
// file_content : Address of the intermediate stores (temporary use) 
// file_length : Address of the length of the file to be played back 
// Return: status : error number, 0 = OK 
// *********************************************************************** 
unsigned char vnc_rd_dir(char *file_name, char *file_content, unsigned int *file_length) 
{ 
 unsigned char status = 0; 
 unsigned int buff_index = 0; 
 char *start; // Address of hex strings (beginning)  
 
 
 sprintf (&vnc.send_buffer[0], "DIR %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 // Response string to DIR is read 
 status = vnc_rd_answer(PROMPT_AND_ERROR_CHECK); 
 
 if (status == PROMPT_OK) 
 { 
 // Response string is now in the buffer (the last character at the forefront) 
 for (buff_index = VNC_BUFFER_SIZE; buff_index; buff_index--) 
 { 
 *file_content = vnc.receive_buffer[buff_index - 1]; // flip the buffer content 
 if (!(*file_content)) *file_content = ' '; // avoid string end identifier (0x00) 
 file_content++; 
 } 
 
 *(file_content) = 0x00; // put string end identifier 
 file_content -= (VNC_BUFFER_SIZE - 2); // Buffer set to start back 
 // Response string is now in the receive buffer, example: "VNCFILE2.TXT $52 $00 $00 $00 D:\>" 
 // char *strchr(const char *s, int c); it locates the first occurrence of c in the string pointed to by s. 
 // it returns a pointer to the byte, or a null pointer if the byte was not found. 
 start = strchr(file_content,'$'); // find the index of the first occurance of $ (starting from LSB) 
 *file_length = str_to_int(start); // convert hex string to a number (currently up to 64 kByte!) 
 if (!(*file_length)) return 51; // error, file is too long or has length of 0 
 return 0; // Ok, valid file length 
 } 
 
 return status; // error number (while 0 = OK) 
} 
 
// *********************************************************************** 
// VNC1L - Generating a binary file on a USB drive 
// (It can also write a text file.) 
// Maximum file length: 64 kByte 
// Parameters: file_name : Address of file_name 
// file_content : Address of data block 
// file_length : nnumber of bytes 
// Return: status : error number, 0 = OK 
// *********************************************************************** 
unsigned char vnc_wr_binfile(char *file_name, char *file_content, unsigned int file_length) 
{  
 
 unsigned int vnc_i = 0; 
 unsigned char wr_steps = 0; 
 unsigned char status = 0; 
 
 if (file_length == 0xFFFF) return 51; // error, file is too long 
 
 do { 
 if (wr_steps == 4) return 0; // done, OK (the last prompt character exists) 
 
 switch (wr_steps) // write process 
 { 
 case 0: // Open or create a file 
 sprintf (&vnc.send_buffer[0], "OPW %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 break; 
 case 1: // Seek offset byte position of 0 in the open file (you can also choose other offset) 
 if (vnc_wr_cmd("SEK 0")) return 1; // Error, command was not accepted 
 break; 
 case 2: // Write the number of bytes to an open file 
 sprintf (&vnc.send_buffer[0], "WRF %d", file_length); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 // No check for buffer limit, if necessary, expand it 
 for(vnc_i = 0; vnc_i < file_length; vnc_i++) 
 { 
 if (vnc_wr_byte(*file_content)) return 1; // Error, command was not accepted 
 file_content++; 
 } 
 break; 
 case 3: // close file 
 sprintf (&vnc.send_buffer[0], "CLF %s", file_name); 
 if (vnc_wr_cmd(vnc.send_buffer)) return 1; // Error, command was not accepted 
 break; 
 } // Switch 
 wr_steps++; // next step 
 status = vnc_rd_answer(PROMPT_AND_ERROR_CHECK); 
 } while (status == PROMPT_OK); // Testing for next prompt character > 
 return status; // error number (while 0 = OK) 
} 