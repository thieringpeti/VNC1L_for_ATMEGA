// Original project by Matthias Kahnt az51@gmx.net 
// Modified by Terry Young Kim 
// ATmega1284 
// VNC1L/VDIP1 Vinculum (Port2) 
// VNC1L - USB HOST CONTROLLER (Mega1284: Master, VNC1L: Slave) 
// VDAP firmware version 3.68 or 3.69 
// SPI interface 
// VDIP1: Jumper-Set: J3 - Pulldown / J4 - Pullup for SPI 
// Adopted for ATMEGA2561 by ThieringPeti 2018
// used a different serial driver for the testing.


 #define F_CPU 16000000
#include <avr/io.h> 
#include <string.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <util/delay.h> // for LCD 
 #include <avr/interrupt.h>

#include "firmware.c" // VNC1L APIs 
#include "serial.h"

 
// pinout: see firmware.h
 
 
volatile char my_text[50]; 
volatile char my_str[50]; 
volatile char read_str[50]; 

 
//********************************************************** 

 
// Textfile 1 
char *file1[] = { 
 "VNC1L USB Drive test\n\r", 
 "It can write to and read from a USB flash drive.\n\r", 
 "Text file 1 VNC1L/VDIP1\n\r\a"}; // \a is the EOF (end of file) 
 
// Textfile 2 
char *file2[] = { 
 "Text file 2,\n\r", 
 "Another text file to be created.\n\r\a" }; 
 
// Textfile 3 
char *file3[] = { 
 "Text file 3 can be appended to an existing file.\n\r\a" }; 
 
// Bin file 
char file4[] = { 
 0xFA,0x89,0x11,0xFF,0xAA,0x02,0x00,0x1F, 
 0x1D,0xBA,0xA6,0x24,0x99,0x2D,0x1E,0x1F, 
 0x0D,0x0C,0xA0,0x55,0x30,0x1F,0x16,0xEF, 
 0x04,0x22,0x00,0x00,0xCF,0x12,0x5F,0xDF, 
 0x69,0x00,0x00,0x1F,0xEF,0x15,0x3F,0xCF, 
 0x5F,0x44,0x1F,0x1F,0xBF,0x16,0x13,0x7F, 
 0x00,0x10,0x1F,0xDF,0x1F,0x8F,0x16,0xDF, 
 0x1F,0x32,0x3F,0x56,0x1F,0x19,0x17,0xFF, 
 0x3F,0x42,0xFE,0x46,0x6F,0x99,0x10,0xAF,  
 0x6F,0x62,0x36,0x66,0x10,0xAA,0xBB,0xCC }; 
 
// Textfile 5 
char *file5[] = { 
 "MacGyver left the building. He ain't coming back.\n\r\a" }; 
 
char file_buffer[FILE_BUFFER_SIZE]; // buffer for testing 
unsigned int file_length; // byte length of a file (for reading) 
 
// system initialization 
void init_system (void) 
{ 
 // LED for debugging 
 LED_DDR |= (1 << LED_PIN); // output 
 LED_D1_OFF; // turn off LED 
 
 // VNC1L SPI interface setup 
 SPI_DDR |= ((1 << SCLK_PIN) | (1 << SDI_PIN) | (1 << CS_PIN) | (1 << RE_PIN)); // outputs 
 SPI_DDR &= ~(1 << SDO_PIN); // input 
 
   serialInit(0, BAUD(250000, F_CPU)); 
   sei();
  serialWriteString(0,"\n\r***** Starting VNC1L USB drive test... *****\n\r"); 
} 
 
// *********************************************************************** 
// List of errors 
// *********************************************************************** 
// 1 = Command was not accepted by VNC1L 
// 2 = Command Failed 
// 3 = Bad Command 
// 4 = Disk Full 
// 5 = Filename Invalid 
// 6 = Read Only 
// 7 = File Open 
// 8 = Directory Not Empty 
// 9 = No Disk 
// 20 = No VNC1L with VDAP firmware available 
// 21 = No USB drive plugged 
// 50 = Buffer is too small to read a file (buffer overflow) 
// 51 = File length = 0 or > 64kByte 
// 99 = Timeout 
// *********************************************************************** 40 
 
void vnc_error_message(unsigned char error_type, unsigned char error_num) 
{ 
 LED_D1_ON; // LED turns on if there's an error 
 
//  fprintf(stdout,"\n\rError type: %d, Error number: %d\n\r", error_type, error_num); 
 
 // error occured 
 while(1); // error keeps the system hanging 
} 
 
// ******************************************************************** 
// Main program 
// ******************************************************************** 
int main (void) 
{ 
 init_system(); 
 
 // start the LCD 
 
 // initialize VNC1L 
 vnc_init(); 
 
 // put some stuff on LCD 
 

 
 // Check for the presence of VDAP firmware on VNC1L 
 if (vnc_wait_for("VDAP")) vnc_error_message(20,1); // check confirmation 

 // Wait until a USB drive is plugged and detected 
 if (vnc_wait_for("Device Detected")) vnc_error_message(21,2); // check confirmation 
 
 // Wait for the first prompt that monitor returns (D:\>) 41 
 
 vnc.status = vnc_prompt_check(); // check confirmation 
 if (vnc.status) vnc_error_message(vnc.status,3); // error message 
 
 // Extented Command Set 
 vnc.status = vnc_wr_cmd("ECS"); 
 if (vnc.status) vnc_error_message(1,4); // error: command was not accepted by VNC1L 
 vnc.status = vnc_prompt_check(); // check if monitor returns prompt 
 if (vnc.status) vnc_error_message(vnc.status,4); 
 // monitor returns prompt if command executed correctly 
 
 // Monitor commands in ASCII: IPA mode 
 vnc.status = vnc_wr_cmd("IPA"); 
 if (vnc.status) vnc_error_message(1,5); // error: command was not accepted by VNC1L 
 vnc.status = vnc_prompt_check(); // check if monitor returns prompt 
 if (vnc.status) vnc_error_message(vnc.status,5); 
 
 // Display firmware version 
 vnc.status = vnc_wr_cmd("FWV"); 
 if (vnc.status) vnc_error_message(1,6); // error: command was not accepted by VNC1L 
 vnc.status = vnc_prompt_check(); // check if monitor returns prompt 
 if (vnc.status) vnc_error_message(vnc.status,6); 
 
 // List files in current directory 
 vnc.status = vnc_wr_cmd("DIR"); 
 if (vnc.status) vnc_error_message(1,7); // error: command was not accepted by VNC1L 
 vnc.status = vnc_prompt_check(); // check if monitor returns prompt 
 if (vnc.status) vnc_error_message(vnc.status,7); 
 
// To print what monitor returns: 
// fprintf(stdout,"\n\r%s\n\r", vnc.rec_buffer); 
// note: All output from the command monitor is LSB first 
// If monitor return multiple lines of data, they are followed by carriage return, 
// which overwrites a previous line on fprintf 
 
 // Write text file 1 to USB disk 
 vnc.status = vnc_wr_txtfile("VNCFILE1.TXT", file1[0], FILE_NEW); 
 
 // if (vnc.status) vnc_error_message(vnc.status,8); 
 
 // Write text file 2 to USB disk 
 vnc.status = vnc_wr_txtfile("VNCFILE2.TXT", file2[0], FILE_NEW); 
 if (vnc.status) vnc_error_message(vnc.status,9); 
 
 // Append text string to existing text file 1 
 vnc.status = vnc_wr_txtfile("VNCFILE1.TXT", file3[0], FILE_APPEND); 
 if (vnc.status) vnc_error_message(vnc.status,10); 
 
 // Write a binary (bin) file to disk 
 vnc.status = vnc_wr_binfile("VNCFILE3.BIN", file4, 80); // file length = 80 
 if (vnc.status) vnc_error_message(vnc.status,11); 
 
 // Before reading a file, you must get the length of it 
 vnc.status = vnc_rd_dir("VNCFILE2.TXT", file_buffer, &file_length); 
 if (vnc.status) vnc_error_message(vnc.status,12); 
 // Now the file length is saved in file_length 
 
 if (!vnc.status) { 
 // Read a text file of known length into file_buffer 
 vnc.status = vnc_rd_file("VNCFILE2.TXT", file_buffer, file_length); 
 if (vnc.status) vnc_error_message(vnc.status,13); 
 } 
 
 // Write text file 5 to disk 
// vnc.status = vnc_wr_txtfile("VNCFILE5.TXT", file5[0], FILE_NEW); 
// if (vnc.status) vnc_fehlermeldung(vnc.status,14); 
 

 
/* 
 // Append a text string to an existing file 
 vnc.status = vnc_wr_txtfile("VNCFILE5.TXT", my_str, FILE_APPEND); 
 // append twice for some reason?!?!?!?!? 
 // my_str seems to cause the problem 
 if (vnc.status) vnc_fehlermeldung(vnc.status,17); 
*/ 
 
 // Append a text string to an existing file 
 vnc.status = vnc_wr_txtfile("VNCFILE5.TXT", file1[0], FILE_APPEND); 
 if (vnc.status) vnc_error_message(vnc.status,17); 
 
 // If you reached here, then no error has occured 
 // LED blinks every second 
 // USB drive can be removed 
 while(1) { 
 LED_D1_ON; 
 _delay_ms(1000); // 1 sec 
 LED_D1_OFF; 
 _delay_ms(1000); 
 } 
} 
 